FROM tensorflow/tensorflow:latest-py3
ADD test1.py /
RUN pip3 install tensorflow
RUN pip3 install keras
RUN pip3 install matplotlib
RUN pip3 install opencv-python
RUN pip3 install numpy
RUN pip3 install pandas
RUN pip3 install pillow
RUN pip3 install seaborn
RUN pip3 install sklearn
RUN pip3 install tensorflow-gpu
RUN pip3 install h5py
ADD pre_trained.h5 /

WORKDIR /dataset

CMD ["python" , "./test1.py"] 	