import tensorflow as tf
from tensorflow.python.keras import layers
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.preprocessing import image
import h5py
import numpy as np
import pandas
import sklearn
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

new_model = load_model('pre_trained.h5')


train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

train_generator = train_datagen.flow_from_directory('ds',
                                                    target_size=(240,240),

                                                    class_mode= 'categorical'
                                                    )
label_map = (train_generator.class_indices)

print(label_map)

img = image.load_img('dataset/test/IMG-20191212-WA0000.jpg', target_size= (240,240))
img  =image.img_to_array(img)
img = np.expand_dims(img, axis=0)
result = new_model.predict(img)
probability = new_model.predict_proba(img)

print(result)

predicted_class_indices=np.argmax(result,axis=1)
labelss = dict((v,k) for k,v in label_map.items())
predictions = [labelss[k] for k in predicted_class_indices]


print("Predicted disease :",predictions)

#print(classification_report(result,probability))
#new_model.fit(result,probability)
print(accuracy_score(result,probability))