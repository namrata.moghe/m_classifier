
import tensorflow as tf
from tensorflow.python import keras
from tensorflow.python.keras import layers, Sequential
from tensorflow.python.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D, Activation,BatchNormalization
from sklearn.metrics import confusion_matrix
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.preprocessing import image
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import numpy as np
import os

from tensorflow.python.keras import regularizers, optimizers
from tensorflow.python.keras.metrics import categorical_crossentropy


import pandas as pd
import numpy as np
img_width , img_height = 240,240


nb_train_samples=2238
nb_validation_samples = 30
epochs = 6
epochs = 6
batch_size = 20

if K.image_data_format()== 'channels_first':
    input_shape = (3,img_width,img_height)
else:
    input_shape = (img_width,img_height,3)


model = Sequential()

model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Flatten())
model.add(Dense(20, activation = 'relu' ))
model.add(Dense(8, activation = 'softmax'))

#model.add(BatchNormalization())



model.compile(loss='categorical_crossentropy',
          optimizer='adam',
          metrics=['accuracy'])

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)



train_generator = train_datagen.flow_from_directory('ds',
                                                    target_size=(img_width,img_height),
                                                    batch_size = batch_size,
                                                    class_mode= 'categorical'
                                                    )

label_map = (train_generator.class_indices)


hist = model.fit_generator(
                    train_generator,
                    steps_per_epoch =nb_train_samples,
                    epochs = epochs,

                    )

model.save('pre_trained.h5')